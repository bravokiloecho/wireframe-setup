module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    asset_path: 'assets/'
    public_path: 'public/'
    view_path: 'views/'
    data_path: 'data/'
    port: '4000'

    assemble:
      options:
        assets: "<%= public_path %>"
        engine: 'jade'
        data: ['<%= data_path %>*.yml','<%= data_path %>*.json']
        flatten: false  
        plugins: [ 'assemble-markdown-import' ]
        basedir: 'views/'
      
        
      dev:
        options:
          production: false
          prettify: true
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'dev/'
      
      public:
        options:
          production: true
          prettify: false
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'public/'

    
    watch:
      sass:
        files: [
          "<%= asset_path %>sass/*"
          "<%= asset_path %>sass/*/*"
        ]
        tasks: ["compass:dev"]
        options:
          atBegin: false

      coffee:
        files: [
          "<%= asset_path %>coffee/*"
        ]
        tasks: ["coffee"]
        options:
          atBegin: false

      assemble:
        files: [
          "<%= view_path %>*"
          "<%= view_path %>*/*"
          "<%= data_path %>*"
          "text/*"
        ]
        tasks: ["assemble:dev"]

      watchFiles:
        files: [
          "<%= public_path %>js/mouse/*"
          "dev/*"
        ]
        options:
          livereload: true

    browserSync:
      options:
        watchTask: true
        port: "<%= port %>"

      files:
        src: [
          "<%= public_path %>css/mouse/*.css"
          "<%= public_path %>gfx/*"
          "<%= public_path %>gfx/*/*"
          "<%= public_path %>images/*"
        ]

    uglify:
      plugins:
        options:
          mangle: true
          beautify: false
          compress: true

        files:
          "<%= public_path %>js/plugins.js": ["<%= asset_path %>plugins/*.js"]

      
      # combines plugins and scripts
      combine_all:
        options:
          mangle: true
          beautify: false
          compress:
            drop_console: true

        files:
          "<%= public_path %>js/min/scripts.js": [
            "<%= public_path %>js/plugins.js"
            "<%= public_path %>js/mouse/scripts.js"
          ]

    compass:
      dev:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/mouse"
          outputStyle: "nested"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: true

      dist:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/min"
          outputStyle: "compressed"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: false

    coffee:
      compileBare:
        options:
          bare: true
          join: true
          sourceMap: true

        files:
          "<%= public_path %>js/mouse/scripts.js": ["<%= asset_path %>coffee/*.coffee"]


    connect:
      server:
        options:
          port: "<%= port %>"
          hostname: "*"


    imagemin: # Task
      dynamic: # Another target
        options:
          cache: false
          optimizationLevel: 2

        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>photos/" # Src matches are relative to this path
          src: ["*.{png,jpg,gif}"] # Actual patterns to match
          dest: "<%= public_path %>photos/" # Destination path prefix
        ]

    image_resize:
      resize:
        options:
          overwrite: true
          upscale: false
          crop: false
          width: 1600
        src: '<%= asset_path %>photos/*.{png,jpg,gif}'
        dest: '<%= public_path %>photos/'


    # Deploy to amazon S3
    # Readme: https://github.com/jpillora/grunt-aws
    aws: grunt.file.readJSON './aws/credentials.json'
    s3:
      options:
        accessKeyId: "<%= aws.accessKeyId %>"
        secretAccessKey: "<%= aws.secretAccessKey %>"
        bucket: "BUCKET_NAME"
        region: 'eu-west-1'
      public:
        cwd: "<%= public_path %>"
        src: "**"


  
  # Load grunt plugins.
  grunt.loadNpmTasks "assemble"
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-browser-sync"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-webfont"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-contrib-imageMin"
  grunt.loadNpmTasks "grunt-image-resize"

  
  #register tasks
  
  #watch with css inject
  grunt.registerTask "default", [
    "connect"
    "browserSync"
    "watch"
  ]
  
  # compile all files that need compiling
  grunt.registerTask "c", [
    "assemble"
    "compass"
    "coffee"
    "uglify:plugins"
    "uglify:combine_all"
  ]

  # DEPLOYMENT
  grunt.registerTask "deploy", [
      "assemble"
      "compass"
      "coffee"
      "uglify:plugins"
      "uglify:legacyPlugins"
      "uglify:combine_all"

      "s3"
  ]


  # COMPILE TASKS
  grunt.registerTask "ass", ["assemble"]
  grunt.registerTask "coff", ["coffee"]
  grunt.registerTask "sass", ["compass"]
  grunt.registerTask "plug", ["uglify:plugins"]
  grunt.registerTask "min", ["imagemin"]

  # make icon font
  grunt.registerTask "icons", ["webfont","compass"]