do ->

	dk8 = window.dk8 = window.dk8 || {}

	defineDeviceVariables = ->

		deviceVariables =
			isMobile: if $('#mobileTester').css('visibility') == 'visible' then true else false
			isTablet: if $('#tabletTester').css('visibility') == 'visible' then true else false
			isTouch: if Modernizr.touch then true else false

		deviceVariables.isDevice = if deviceVariables.isMobile or deviceVariables.isTablet then true else false

		return deviceVariables
	
	# Returns a random float or integer between min (inclusive) and max (inclusive)
	getRandomNumber = ( min, max, integer ) ->
		
		if integer == false
			return Math.random() * ( max - min ) + min
		
		return Math.floor(Math.random() * (max - min + 1)) + min


	# SWAP SVGs with PNGs
	svgFallback = (svgSupport) ->
		if !svgSupport
			$('img[src*="svg"]').attr 'src', ->
				$(this).attr('src').replace('.svg', '.png')

	randomString = (length, chars) ->
		mask = ""
		mask += "abcdefghijklmnopqrstuvwxyz"  if chars.indexOf("a") > -1
		mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"  if chars.indexOf("A") > -1
		mask += "0123456789"  if chars.indexOf("#") > -1
		mask += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\"  if chars.indexOf("!") > -1
		result = ""
		i = length

		while i > 0
			result += mask[Math.round(Math.random() * (mask.length - 1))]
			--i

		return result

	placeholder = (els,activeClass = "active") ->
		$(els).each ->
			$el = $(this)
			placeholder = $el.data().placeholder
			$el.val placeholder
			if $.trim($el.val()) is ""
				$el
					.val placeholder
					.removeClass activeClass
			$el.focus(->
				if $el.val() is placeholder
					$el
						.val ""
						.addClass activeClass
			).blur ->
				if $.trim($el.val()) is ""
					$el
						.val placeholder
						.removeClass activeClass

	trackEvent = (category,action,label = 'undefined',value = 0) ->
		
		if typeof ga == 'undefined'
			# console.log 'Track event: '
			# console.log ' category: '+category
			# console.log ' action: '+action
			# console.log ' label: '+label
			return

		ga(
			'send',
			'event',
			category,
			action,
			label,
			value
		)

	preloadImage = ( src ) ->
		img = new Image()
		img.src = src

	
	# http://stackoverflow.com/questions/5899783/detect-safari-chrome-ie-firefox-opera-with-user-agent
	browserDetect = ->
		browser = 
			chrome: navigator.userAgent.indexOf('Chrome') > -1
			explorer: navigator.userAgent.indexOf('MSIE') > -1
			firefox: navigator.userAgent.indexOf('Firefox') > -1
			safari: navigator.userAgent.indexOf("Safari") > -1
			opera: navigator.userAgent.toLowerCase().indexOf("op") > -1
		
		if browser.chrome and browser.safari
			browser.safari = false
		if browser.chrome and browser.opera 
			browser.chrome = false

		return browser


	# plugin for selecting text
	# from: http://is.gd/E1kgQ7
	jQuery.fn.selectText = ->
		doc = document
		element = this[0]
		range = undefined
		selection = undefined
		if doc.body.createTextRange
			range = document.body.createTextRange()
			range.moveToElementText element
			range.select()
		else if window.getSelection
			selection = window.getSelection()
			range = document.createRange()
			range.selectNodeContents element
			selection.removeAllRanges()
			selection.addRange range

	# plugin for setting opacity
	jQuery.fn.opacity = ( o ) ->
		@each ->
			$(this).css opacity: o

	# Degrees to radians
	degToRad = ( deg ) ->
		return deg * (Math.PI / 180)


	# EXPORT
	dk8.svgFallback = svgFallback
	dk8.randomString = randomString
	dk8.placeholder = placeholder
	dk8.trackEvent = trackEvent
	dk8.getRandomNumber = getRandomNumber
	dk8.preloadImage = preloadImage
	dk8.defineDeviceVariables = defineDeviceVariables
	dk8.degToRad = degToRad
	dk8.browserDetect = browserDetect