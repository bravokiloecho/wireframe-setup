do ->

	dk8 = window.dk8 = window.dk8 || {}
	SITE = window.SITE = window.SITE || {}


	# define empty page vars
	
	# GLOBAL
	$window = null
	$body = null
	$wrapper = null
	WIDTH = null
	HEIGHT = null
	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	# SPECIFIC TO HERE

	
	defineVars = ->
		gv = window.gv
		$window = gv.$window
		$body = gv.$body
		$wrapper = gv.$wrapper
		WIDTH = gv.WIDTH
		HEIGHT = gv.HEIGHT

		isMobile = gv.deviceVariables.isMobile
		isTablet = gv.deviceVariables.isTablet
		isDevice = gv.deviceVariables.isDevice
		isTouch = gv.deviceVariables.isTouch

		# SPECIFIC TO HERE


	initMain = ->
		do defineVars
		
		do onResize
		$window.on 'resize' , ->
			do onResize
			do debouncedResize




	
	toggleNavElementsOnMousemove = ->
		delay = 3000
		showing = true

		timer = setTimeout(->
			if $homepageNavElements.filter('.hovered').length || $projectLinksContainer.hasClass 'open'
				return
			$homepageNavElements.addClass 'hidden'
			showing = false
		, delay)

		$body.on 'mousemove', ->
			if timer
				clearTimeout timer
				timer = 0

			if !showing
				$homepageNavElements.removeClass 'hidden'

			showing = true

			timer = setTimeout(->
				if $homepageNavElements.filter('.hovered').length || $projectLinksContainer.hasClass 'open'
					return
				$homepageNavElements.addClass 'hidden'
				showing = false
			, delay)


	setHoverClassOnHomeElements = ->
		$body.on 'mouseenter', '.homepage-nav-element', ->
			$(this).addClass 'hovered'
		$body.on 'mouseleave', '.homepage-nav-element', ->
			$(this).removeClass 'hovered'


	toggleTopInstructions = ( type, switchTo ) ->
	
		$all = $topInstructions.children()

		if !type
			$all.hide()
			return
		
		$text = $topInstructions.children '#' + type + '-instructions'

		# TURN ON
		if switchTo # == 1
			$text.show()
			return
		# TURN OFF
		else
			$text.hide()

	# SLIDESHOW
	# #########

	initHomepageSlider = ( $slider ) ->

		# Set slider width
		slideCount = $slider.children('.slide-wrapper').length

		# ignore final slide if device
		if isDevice
			slideCount = slideCount - 1

		$slider.css
			width: ( 100 * slideCount ) + '%'

		if isDevice
			return

		# Set initial slideshow data
		$slider
		.data 'currentslide', 0
		.data 'totalslides', slideCount

		# Remove slide instructions on first slide change
		$slider.one 'slide-change', ->
			$topInstructions.find('#slideshow-instructions').remove()

		do initSlideshowControls


	cycle = ( direction, $slider ) ->

		currentSlide = $slider.data 'current-slide'
		totalSlides = $slider.data 'total-slides'
		increment = 1

		if direction is 'prev'
			increment = -1
		

		currentSlide = (currentSlide + increment) % totalSlides

		dk8.trackEvent 'slideshow', 'cycle', direction

		$slider.data 'current-slide', currentSlide
	
	# MISC FUNCTIONS
	# ##############

	keyboardShortcuts = ( $slider, $imagePanel ) ->
		
		# Listen to left / right keys
		Mousetrap.bind [ 'left' ] , (e) ->
			cycle 'prev'
			return

		Mousetrap.bind [ 'right' ] , (e) ->
			cycle 'next'
			return

		if $.fullscreen.isNativelySupported()
			Mousetrap.bind [ 'f' ] , (e) ->
				do SITE.toggleFullscreen
				return

	toggleNoScroll = ->
		$body.toggleClass 'no-scroll'


	
	# RESIZE SCRIPTS
	# ##############
	

	onResize = ->
		WIDTH = gv.WIDTH = $window.width()
		HEIGHT = gv.HEIGHT = $window.height()


	debouncedResize = $.debounce 250 , ->
		# do something


	SITE.initMain = initMain