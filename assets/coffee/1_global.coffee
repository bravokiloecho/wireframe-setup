do ->

	dk8 = window.dk8 = window.dk8 || {}
	SITE = window.SITE = window.SITE || {}

	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	$html = null
	$body = null
	$wrapper = null
	
	initGlobal = ->
		# DEFINE GLOBAL VARIABLES HERE
		gv = window.gv =
			$window: $(window)
			$document: $(document)
			$html: $('html').first()
			$body: $('body').first()
			WIDTH: $(window).width()
			HEIGHT: $(window).height()
			deviceVariables: dk8.defineDeviceVariables()
			isDev: window.isDev
			$wrapper: $ '#wrapper'
			# browser: dk8.browserDetect()

		$html = gv.$html
		$body = gv.$body
		$wrapper = gv.$wrapper

		isMobile = gv.deviceVariables.isMobile
		isTablet = gv.deviceVariables.isTablet
		isDevice = gv.deviceVariables.isDevice
		isTouch = gv.deviceVariables.isTouch

		do backToTopOnClick
		do logLinkClicks
		do testLegacyWarning
		do setDeviceBodyClasses

		# FASTCLICK
		FastClick.attach document.body


	logLinkClicks = ->
		$body.on 'click', 'a', ->
			href = $(this).attr 'href'
			Class = $(this).attr 'class'
			label = if href then href else Class
			# category, action, label
			dk8.trackEvent 'linkClick', 'click', label


	backToTopOnClick = ->
		$wrapper.on 'click', '.back-to-top', ->
			scrollTo 0

	toggleFullscreen = ->

		# Uses fullscreen plugin
		# https://github.com/private-face/jquery.fullscreen

		isFullscreen = $.fullscreen.isFullScreen()

		if !isFullscreen
			gv.$body.fullscreen()
		else
			$.fullscreen.exit()

	scrollTo = ( offset, speed, $target ) ->
		
		# Uses velocity.js

		offset = offset || 0
		speed = speed || 200
		$target = $target || $ 'html'

		$target.velocity 'scroll',
			duration: speed
			offset: offset
			easing: 'ease-out'


	setDeviceBodyClasses = ->
		
		if isMobile
			$body
			.addClass 'is-mobile'
			.addClass 'is-device'
			return

		if isTablet
			$body
			.addClass 'is-tablet'
			.addClass 'is-device'
			return


	testLegacyWarning = ->
		$html = $('html').first()
		ieTest = $html.attr('data-ie')
		
		if ieTest is 'ie7'
			$html.addClass 'ie7'
		else if ieTest is 'ie8'
			$html.addClass 'ie8'
		else if ieTest is 'ie9'
			$html.addClass 'ie9'

			
	SITE.initGlobal = initGlobal
	SITE.toggleFullscreen = toggleFullscreen
	SITE.scrollTo = scrollTo